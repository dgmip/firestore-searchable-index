import * as S from "../../src/index";

const correctSearchableIndex = {
  "b": true,
  "be": true,
  "ben": true,
  "ben ": true,
  "ben w": true,
  "ben wa": true,
  "ben wal": true,
  "ben wall": true,
  "w": true,
  "wa": true,
  "wal": true,
  "wall": true,
};

const correctArrayIndex = {
  "p": true,
  "pe": true,
  "pet": true,
  "pete": true,
  "peter": true,
  "peter ": true,
  "peter w": true,
  "peter wi": true,
  "peter wil": true,
  "peter will": true,
  "peter willi": true,
  "peter willia": true,
  "peter william": true,
  "peter williams": true,
  "w": true,
  "wi": true,
  "wil": true,
  "will": true,
  "willi": true,
  "willia": true,
  "william": true,
  "williams": true,
  ...correctSearchableIndex,
};

let searchableIndex: any;

describe("Searchable index", () => {

  beforeEach(() => {
    searchableIndex = S.firestoreSearchableIndex;
  });

  it(`should create correct index for 'Ben Wall'`, () => {
    const ourIndex = searchableIndex.create("Ben Wall");
    expect(ourIndex).toEqual(correctSearchableIndex);
  });

  it("should create correct index with an array of names", () => {
    const ourIndex = searchableIndex.create(["Peter Williams", "Ben Wall"]);
    expect(ourIndex).toEqual(correctArrayIndex);
  });
});
