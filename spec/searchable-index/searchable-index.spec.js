"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var S = __importStar(require("../../src/index"));
var correctSearchableIndex = {
    "b": true,
    "be": true,
    "ben": true,
    "ben ": true,
    "ben w": true,
    "ben wa": true,
    "ben wal": true,
    "ben wall": true,
    "w": true,
    "wa": true,
    "wal": true,
    "wall": true,
};
var correctArrayIndex = __assign({ "p": true, "pe": true, "pet": true, "pete": true, "peter": true, "peter ": true, "peter w": true, "peter wi": true, "peter wil": true, "peter will": true, "peter willi": true, "peter willia": true, "peter william": true, "peter williams": true, "w": true, "wi": true, "wil": true, "will": true, "willi": true, "willia": true, "william": true, "williams": true }, correctSearchableIndex);
var searchableIndex;
describe("Searchable index", function () {
    beforeEach(function () {
        searchableIndex = S.firestoreSearchableIndex;
    });
    it("should create correct index for 'Ben Wall'", function () {
        var ourIndex = searchableIndex.create("Ben Wall");
        expect(ourIndex).toEqual(correctSearchableIndex);
    });
    it("should create correct index with an array of names", function () {
        var ourIndex = searchableIndex.create(["Peter Williams", "Ben Wall"]);
        expect(ourIndex).toEqual(correctArrayIndex);
    });
});
