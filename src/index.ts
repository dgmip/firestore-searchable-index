export interface ISearchableIndex {
  [term: string]: boolean;
}

class SearchableIndex {
  public terms: string | string[] = [];

  public create(terms: string | string[]): ISearchableIndex {
    this.terms = (Array.isArray(terms)) ? terms : [terms];
    const searchableIndex: ISearchableIndex = {};

    let prevKey = "";
    // for individual words in the string
    this.terms.forEach((tagString) => {
      // add the whole string to the search index
      prevKey = "";
      for (const char of tagString) {
        const key = prevKey + char;
        searchableIndex[key.toLowerCase()] = true;
        prevKey = key;
      }

      // add each word to the index
      const arr2 = tagString.toLowerCase().split(" ");
      for (const word of arr2) { // for each word
        const arr3 = word.split("");
        let prevWordKey = "";
        for (const char of arr3) { // for each character
          const key = prevWordKey + char;
          searchableIndex[key.toLowerCase()] = true;
          prevWordKey = key;
        }
      }
    });
    return searchableIndex;
  }

}

export const firestoreSearchableIndex = new SearchableIndex();
