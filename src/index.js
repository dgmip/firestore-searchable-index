"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SearchableIndex = /** @class */ (function () {
    function SearchableIndex() {
        this.terms = [];
    }
    SearchableIndex.prototype.create = function (terms) {
        this.terms = (Array.isArray(terms)) ? terms : [terms];
        var searchableIndex = {};
        var prevKey = "";
        // for individual words in the string
        this.terms.forEach(function (tagString) {
            // add the whole string to the search index
            prevKey = "";
            for (var _i = 0, tagString_1 = tagString; _i < tagString_1.length; _i++) {
                var char = tagString_1[_i];
                var key = prevKey + char;
                searchableIndex[key.toLowerCase()] = true;
                prevKey = key;
            }
            // add each word to the index
            var arr2 = tagString.toLowerCase().split(" ");
            for (var _a = 0, arr2_1 = arr2; _a < arr2_1.length; _a++) { // for each word
                var word = arr2_1[_a];
                var arr3 = word.split("");
                var prevWordKey = "";
                for (var _b = 0, arr3_1 = arr3; _b < arr3_1.length; _b++) { // for each character
                    var char = arr3_1[_b];
                    var key = prevWordKey + char;
                    searchableIndex[key.toLowerCase()] = true;
                    prevWordKey = key;
                }
            }
        });
        return searchableIndex;
    };
    return SearchableIndex;
}());
exports.firestoreSearchableIndex = new SearchableIndex();
