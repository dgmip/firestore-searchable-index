# Firestore Searchable Indexc
  Create searchable indices in firestore from a string or string[]

## Installation
    npm i --save @dgmip/firestore-searchable-index

## Use
    import * as searchableIndex from "@dgmip/firestore-searchable-index";

    mySearchableIndex = searchableIndex.firestoreSearchableIndex;

    // create and index on a string
    const index = searchableIndex.create("Ben Wall");
    // create and index on an array
    const index = searchableIndex.create(["Ben Wall", "Peter Williams"]);

## Example search using @angular/fire
    offset = new Subject<string>() // your search term subject
    results$: Observable<any[]>

    contructor() {
      this.results$ = this.search()
    }

    search() {
      return this.offset.pipe(
        filter(val => !!val), // filter empty strings
        switchMap(offset => {
          return this.afs.collection('names', ref =>
            ref.orderBy(`searchableIndex.${ offset }`)
          )
            .snapshotChanges().pipe(
              map(actions => {
                const results = []
                actions.forEach(action => {
                  const id = action.payload.doc.id
                  const data = action.payload.doc.data()
                  results.push({ id, data })
                })
                return results
              })
            )
        })
      )
    }
    
    // fire off a search for 'ben'
    fireSearch() {
      this.offset.next('ben')
    }
